import token_artifacts from '../../build/contracts/LeonardianCrowdsale.json';
 
import _token_artifacts from '../../build/contracts/LeonardianToken.json';
 
var BigNumber = require('bignumber.js');
var web3;
var accounts;
var owner;
var contract;
var token_contract;
var truffleDevRpc = 'http://127.0.0.1:7545/';
// var truffleDevRpc = 'https://ropsten.infura.io/KbQuP7xkP1ZYNhJkUOXF';
var token_address = '0x3b7db1d8684e6a513bcf8396d061fa2accdc8e64';
var truffleDevContractAddress = '0xce7acf4d7d08b124a37716f9e770c25c6ee84b05';

window.App = {
  start: function() {
    contract = web3.eth.contract(token_artifacts.abi).at(truffleDevContractAddress);

    token_contract = web3.eth.contract(_token_artifacts.abi).at(token_address);

    var events = token_contract.allEvents();   

    // watch for changes
    events.watch(function(error, event){
      if (!error) {
        // console.log('----------------------- EVENT INFO ---------------------------- ')
        // console.log(event);
        // console.log('----------------------- END EVENT INFO ---------------------------- ')
      }
      if (error) {
        // console.log('error: ');
        // console.log(error);
      }
    });

    // Get the initial accounts
    web3.eth.getAccounts(function(err, accs) {
      if (err !== null || accs.length === 0) {
        alert(
          'There was an error fetching your accounts. ' +
          'Make sure the Truffle Developer Ethereum client is running.'
        );
        return;
      }

      accounts = accs;
      owner = accounts[0];

      App.info();
    });
  },

  info: function() {
      var text = '';

      var owner = contract.owner.call();

      text += '<b>Contract owner:</b> ' + owner + '<br />';
      text += '<b>MAX Tokens:</b> ' + web3.fromWei(contract.maxTokens.call(), 'ether') + '<br />';
      text += '<b>Tokens preICO:</b> ' + web3.fromWei(contract.preIcoTokens.call(), 'ether') + '<br />';
      text += '<b>Owner tokens amount:</b> ' + web3.fromWei(token_contract.balanceOf.call(owner).toNumber(), 'ether') + '<br />';
      text += '<b>Raised funds:</b> ' + web3.fromWei(contract.weiRaised.call(), 'ether') + '<br />';

      // var stage = contract.stage.call();

      // if (stage == 1) {
      //     text += '<b>Stage:</b> ICO <br />';
      // } else {
      //   text += '<b>Stage:</b> preICO <br />';
      // }

      text += '<b>Total tokens sold:</b> ' + web3.fromWei(token_contract.totalSupply.call(), 'ether') + '<br />';
      text += '<b>Token address:</b> ' + contract.token.call() + '<br />';

      text += '<b>Current User tokens amount:</b> ' + web3.fromWei(token_contract.balanceOf.call(window.web3.eth.defaultAccount).toNumber()) + '<br />';

      text += '<b>Current metamaks address:</b> ' + window.web3.eth.defaultAccount + '<br />';
 
      document.getElementById('info').innerHTML = text;

      // App.getTransactionsByAccount(window.web3.eth.defaultAccount);
  },
  transfer: function() { // this can be executed only by the owner
      var transferFromAddress = document.getElementById('transferFromAddress').value;
      var transferToAddress = document.getElementById('transferToAddress').value;
      var transferAmount = document.getElementById('transferAmount').value;
      
      var transfer = contract.mint(transferFromAddress, web3.toWei(transferAmount, "ether"));

      console.log(transfer);

      token_contract.transfer(transferToAddress, web3.toWei(transferAmount, "ether"));

      var tokenAmount = token_contract.balanceOf(transferToAddress);

      document.getElementById('transferStatus').innerHTML = 'Receiver tokens amount:' + web3.fromWei(tokenAmount).toNumber();

  },
  getTransactionsByAccount: function(myaccount, startBlockNumber, endBlockNumber) {

    return false;

    if (endBlockNumber == null) {
      endBlockNumber = web3.eth.blockNumber;
      console.log("Using endBlockNumber: " + endBlockNumber);
    }
    if (startBlockNumber == null) {
      startBlockNumber = endBlockNumber - 1000;
      console.log("Using startBlockNumber: " + startBlockNumber);
    }
    console.log("Searching for transactions to/from account \"" + myaccount + "\" within blocks "  + startBlockNumber + " and " + endBlockNumber);

    for (var i = startBlockNumber; i <= endBlockNumber; i++) {
      if (i % 1000 == 0) {
        console.log("Searching block " + i);
      }
      var block = web3.eth.getBlock(i, true);
      if (block != null && block.transactions != null) {
        block.transactions.forEach( function(e) {
          if (myaccount == "*" || myaccount == e.from || myaccount == e.to) {
            console.log("  tx hash          : " + e.hash + "\n"
              + "   nonce           : " + e.nonce + "\n"
              + "   blockHash       : " + e.blockHash + "\n"
              + "   blockNumber     : " + e.blockNumber + "\n"
              + "   transactionIndex: " + e.transactionIndex + "\n"
              + "   from            : " + e.from + "\n" 
              + "   to              : " + e.to + "\n"
              + "   value           : " + e.value + "\n"
              + "   time            : " + block.timestamp + " " + new Date(block.timestamp * 1000).toGMTString() + "\n"
              + "   gasPrice        : " + e.gasPrice + "\n"
              + "   gas             : " + e.gas + "\n"
              + "   input           : " + e.input);
          }
        })
      }
    }
  },
  buy: function() {
    var address = window.web3.eth.defaultAccount;
    var amount = buyAmount.value;

    var send = web3.eth.sendTransaction({from:address,to:truffleDevContractAddress, value:web3.toWei(amount, "ether"), gas: 900000, gasPrice: 100}, function(r, e){
          console.log(e);
          console.log(r);
      });

    App.info();
  },
};

window.addEventListener('load', function() {
  let providerURI = truffleDevRpc;
  let web3Provider = new Web3.providers.HttpProvider(providerURI);
  web3 = new Web3(web3Provider);
  web3.eth.defaultAccount = window.web3.eth.defaultAccount;

  App.start();
});

var walletAddress = document.getElementById('walletAddress');
var balanceButton = document.getElementById('balanceButton');
var balanceLabel = document.getElementById('Balance');
var transferFromAddress = document.getElementById('transferFromAddress');
var transferToAddress = document.getElementById('transferToAddress');
var transferAmount = document.getElementById('transferAmount');
var transferButton = document.getElementById('transferButton');
var transferStatus = document.getElementById('transferStatus');
var buyCrowdsaleName = document.getElementById('buyCrowdsaleName');
var beneficiaryAddress = document.getElementById('beneficiaryAddress');
var buyAmount = document.getElementById('buyAmount');
var buyButton = document.getElementById('buyButton');
var buyStatus = document.getElementById('buyStatus');
var crowdsaleStatus = document.getElementById('crowdsaleStatus');
var crowdsaleName = document.getElementById('crowdsaleName');
var smsStatus = document.getElementById('smsStatus');

transferButton.onclick = window.App.transfer;
buyButton.onclick = window.App.buy;
