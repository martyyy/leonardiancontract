require("babel-core/register");
require("babel-polyfill");

var LeonardianCrowdsale = artifacts.require("./LeonardianCrowdsale.sol");
var LeonardianToken = artifacts.require("./LeonardianToken.sol");

contract('Crowdsale', function() {

	it("should buy token", async () => {
		const account1  = web3.eth.accounts[1];
		const amount = web3.toWei('0.01', 'ether');

		let crowdsale = await LeonardianCrowdsale.deployed();
		let tx = await crowdsale.buyTokens(account1, {value: amount, from: account1});
	});

	it('one ETH should buy 2500 LEON Tokens in PreICO', function(done){
	    LeonardianCrowdsale.deployed().then(async function(instance) {
	        const data = await instance.sendTransaction({ from: web3.eth.accounts[7], value: web3.toWei(1, "ether")});
	        const tokenAddress = await instance.token.call();
	        const leonardianToken = LeonardianToken.at(tokenAddress);
	        const tokenAmount = await leonardianToken.balanceOf(web3.eth.accounts[7]);
	       
	        assert.equal(tokenAmount.toNumber(), 2500000000000000000000, 'The sender didn\'t receive the tokens as per PreICO rate');
	        done();
	   });
	});

	it('should owner get tokens if contributer sent more than 3 ETH', function(done){
		LeonardianCrowdsale.deployed().then(async function(instance) {
			const owner = await instance.owner.call();
			const data = await instance.sendTransaction({ from: web3.eth.accounts[7], value: web3.toWei(3, "ether")});
			
			const tokenAddress = await instance.token.call();
	        const leonardianToken = LeonardianToken.at(tokenAddress);
	        const tokenAmount = await leonardianToken.balanceOf(owner);

			assert.equal(tokenAmount.toNumber(), 7500000000000000000000, 'Owner dont own 7500 tokens after 3 ETH contribution');
			done();
		});
	});

	it('should transfer 1000 LEONS to new account', function(done){
		LeonardianCrowdsale.deployed().then(async function(instance) {

			const token_amount = 1000000000000000000000;

			const owner = await instance.owner.call();
			const tokenAddress = await instance.token.call();
	        const leonardianToken = LeonardianToken.at(tokenAddress);
	        const mint = await instance.mint(owner, token_amount);

	        const transfer = await leonardianToken.transfer(web3.eth.accounts[8], token_amount);

	        const tokenAmount = await leonardianToken.balanceOf(web3.eth.accounts[8]);

			assert.equal(tokenAmount.toNumber(), token_amount, 'New account dont own 1000 LEONS after transfer');
			done();
		});
	});

});