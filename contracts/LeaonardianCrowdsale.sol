pragma solidity ^0.4.18;

import 'zeppelin-solidity/contracts/crowdsale/validation/TimedCrowdsale.sol';
import "zeppelin-solidity/contracts/crowdsale/emission/MintedCrowdsale.sol";
import "zeppelin-solidity/contracts/ownership/Ownable.sol";
import './LeonardianToken.sol';

contract LeonardianCrowdsale is MintedCrowdsale, TimedCrowdsale, Ownable {

	using SafeMath for uint;

	uint public maxTokens = 263000000  * (10 ** 18); // There will be total 500mil LEON Tokens
	uint public preIcoTokens = 7500000 * (10 ** 18); // 7,5mil LEONS will be sold in Crowdsale

	uint public MaxWeiContribute = 3000000000000000000;
 
	function LeonardianCrowdsale(
		uint _startTime,
		uint _endTime,
		uint _rate,
		address _wallet,
		MintableToken _token) public 
	Crowdsale(_rate, _wallet, _token)
	TimedCrowdsale(_startTime, _endTime)
	{		

	}

	function _updatePurchasingState(address _beneficiary, uint256 _weiAmount) internal {

		uint _token_amount = _getTokenAmount(_weiAmount);

		preIcoTokens = preIcoTokens.sub(_token_amount);
	}

	// We need this function for transfer tokens to users after their KYC verification
	function mint(address _to, uint256 _amount) public onlyOwner {
		_deliverTokens(_to, _amount);
	}

	function finalize() public onlyOwner {

		uint rest_tokens_after_sale = maxTokens.sub(token.totalSupply());

		if (preIcoTokens > 0) {
			rest_tokens_after_sale.add(preIcoTokens); 			
		}

		_deliverTokens(msg.sender, rest_tokens_after_sale);
	}	
}