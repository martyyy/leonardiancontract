pragma solidity ^0.4.18;

import 'zeppelin-solidity/contracts/token/ERC20/MintableToken.sol';

/**
 * The LeonardianToken contract does this and that...
 */
contract LeonardianToken is MintableToken  {

	string public constant name = "Leonardian"; 
	string public constant symbol = "LEON";
	uint8 public constant decimals = 18;
}
