var HDWalletProvider = require("truffle-hdwallet-provider");
var infura_apikey = "KbQuP7xkP1ZYNhJkUOXF"; // Either use this key or get yours at https://infura.io/signup. It's free.


//MUST BE CHANGED WHEN UPLOAD TO REAL ETN
var mnemonic = "candy maple cake sugar pudding cream honey rich smooth crumble sweet treat";

module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 7545,
      gas: 6500000,
      network_id: "5777",
    },
    ropsten:  {
      provider: new HDWalletProvider(mnemonic, "https://ropsten.infura.io/" + infura_apikey),
      network_id: 3,
      gas: 4500000,
      // from: "0x6330a553fc93768f612722bb8c2ec78ac90b3bbc"
    },
    live: {
      host: "127.0.0.1",
      port: 8546,
      network_id: 1,
      gas: 3721975,
      // gasPrice: 2000000000,
      from:"0x8131afb36d132578b99d4766e95f4481c6661ec4"

    }
  },
  rpc: {
    host: "127.0.0.1",
    port: 8545
  },
  solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  }
};