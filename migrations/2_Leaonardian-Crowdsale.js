var LeonardianCrowdsale = artifacts.require("./LeonardianCrowdsale.sol");
var LeonardianToken = artifacts.require("./LeonardianToken.sol");

module.exports = function(deployer, network, accounts) {
    
    const startTime = Math.round((new Date(Date.now() - 86400000).getTime())/1000); // Yesterday
    const endTime = Math.round((new Date().getTime() + (86400000 * 28))/1000); // Today + 28 days
    var exchangeRate = 2500; // 1 LEON = 0.0025 ETH or 1 ETH = 2500 LEON

    return deployer
        .then(() => {
            return deployer.deploy(LeonardianToken);
        })
        .then(() => {

            return deployer.deploy(
                LeonardianCrowdsale,
                startTime,
                endTime,
                exchangeRate,
                "0x57E24c76e55dfB89Bc395bae4e5b073D33C566C3",
                LeonardianToken.address
            );
        })
        .then(() => {

            var token = LeonardianToken.at(LeonardianToken.address);

            token.transferOwnership(LeonardianCrowdsale.address);
        });
};








